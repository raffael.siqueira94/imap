'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    title: {
        type: String,
        required: [true, 'The title is required!'],
        trim: true
    },
    content: {
        type: JSON,
        required: [true, 'The content is required!']
    }
});

module.exports = mongoose.model('Map', schema);