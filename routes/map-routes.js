'use strict'

const express = require('express');
const router = express.Router();
const controller = require('../controllers/map-controller')
const authService = require('../services/auth-service');

router.get('/', controller.get);
router.get('/:title', controller.getByTitle);
//router.post('/', authService.authorize, controller.post);  
router.post('/', controller.post);
router.put('/', controller.put);
router.delete('/', controller.delete);

module.exports = router;