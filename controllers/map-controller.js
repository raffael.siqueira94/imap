'use strict';

const ValidationContract = require('../validators/fluent-validator');
const repository = require('../repositories/map-repository');

exports.get = async(req, res, next) => {
    try {
        var data = await repository.get();
        res.status(200).send(data);
    } catch (e){
        res.status(500).send({
            message: 'Falha ao processar sua requisição'
        });
    }
};

exports.getByTitle = async(req, res, next) => {
    try{
        var data = await repository.getByTitle(req.params.title);
        //console.log(data);
        res.status(200).send(data);
    }
    catch(e){
        res.status(500).send({
            message: 'Falha ao processar sua requisição'
        });
    }
    
};

exports.post = async(req, res, next) => {
    try{
        await repository.create(req.body);
        res.status(201).send({
            message: 'Mapa cadastrado com sucesso!'
        });
    }
    catch(e){
        res.status(500).send({
            message: 'Falha ao processar requisição'
        });
    }
};

exports.put = async(req, res, next) => {
    try{
        await repository.update(req.body);
        res.status(200).send({
            message: 'Mapa atualizado com sucesso!'
        });
    } catch (e){
        res.status(500).send({
            message: 'Falha ao processar sua requisição'
        });
    }
    
  };

  exports.delete = async(req, res, next) => {
    try{
        await repository.delete(req.body.id);
        res.status(200).send({
            message: 'Mapa removido com sucesso!'
        });
    } catch(e){
        res.status(500).send({
            message: 'Falha ao remover mapa',
            data: e
        });
    }
  };