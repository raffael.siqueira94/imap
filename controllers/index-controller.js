'use strict';

const ValidationContract = require('../validators/fluent-validator');
const repository = require('../repositories/map-repository');

exports.get = async(req, res, next) => {
    try {
        var data = await repository.get();
        res.status(200).send(data);
    } catch (e){
        res.status(500).send({
            message: 'Falha ao processar sua requisição'
        });
    }
};

exports.getByTitle = async(req, res, next) => {
    try{
        var data = await repository.getByTitle(req.params.title);
        console.log(data[0].id);
        res.status(200).send(data);
    }
    catch(e){
        var data = await repository.get();
        res.status(200).send(data);
    }
    
};

exports.post = async(req, res, next) => {
    console.log(req.body);
    try{
        await repository.create(req.body);
        res.status(201).send({
            message: 'Mapa cadastrado com sucesso!'
        });
    }
    catch(e){
        res.status(500).send({
            message: 'Falha ao processar requisição'
        });
    }
};

exports.put = async(req, res, next) => {
    try{
        await repository.update(req.params.id, req.body);
        res.status(200).send({
            message: 'Produto atualizado com sucesso!'
        });
    } catch (e){
        res.status(500).send({
            message: 'Falha ao processar sua requisição'
        });
    }
    
  };

  exports.delete = async(req, res, next) => {
    try{
        await repository.delete(req.body.id);
        res.status(200).send({
            message: 'Mapa removido com sucesso!'
        });
    } catch(e){
        res.status(500).send({
            message: 'Falha ao remover mapa',
            data: e
        });
    }
  };