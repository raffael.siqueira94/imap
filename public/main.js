'use strict';

//Sempre declare variáveis globais fora do escopo local. Senão elas vão resetar e não vai funcionar
var selectedNode = 0;
var nId = 0;
var nAlt = 0;
var nCar = 0;
var altAtual = null;

var NumberNode = null;
var AlternativeCount = 0;
var ScenarioCount = 0;
var ImplicationCount = 0;
var ImpactCount = 0;
var varTooltip = 1;

var x = null;
var y = null;
var z = null;
var json1 = null;
var json2 = null;
var length = null;
var array = [];
var alternativaAtual = 0;
var chatMessages = [];

var chatStatus = 0;

(function() {

  $('#textArea1').prop('disabled', true);

  var socket = io();

  $('#saveMapbtn').on('click', function() {
    if ($('#mapName').val() === ""){
      alert('You need to choose a name for your new map!');
    }
    else{
      window.location.href = "/" + $('#mapName').val();
    }
  })

  $('#closeTutorial').on('click', function(){
    if(window.location.pathname.substr(1) === ""){
      $('#saveMapModal').modal('show');
    }
    
  })

$(window).on('load', function(){
  console.log('oi');
    $.get('/maps' + window.location.pathname, function(resultado){
      if (resultado[0] === undefined){
        $.ajax({
          type: 'POST',
          url: '/maps',
          data: {
            title: window.location.pathname.substr(1),
            content: JSON.stringify(cy.json())
          },
          success: function(response) {
            console.log(response);
          },
          error: function(){
            console.log('error posting map');
          }
        });
      }
      else{
        console.log("Entrou na requisição");
        cy.remove(cy.elements());
        cy.json(JSON.parse(resultado[0].content));
        cy.nodes().forEach(function(node){
          if (node.data('nivel') == 1){
            node.style('shape', 'triangle');
            node.style("background-color","#00FF00");
            node.data('type', 'Scenario');
            }
          else if(node.data('nivel') == 2){
            node.style('shape', 'roundrectangle');
            node.style("background-color","#FF8C00"); 
            node.data('type', 'Alternative');
          }
          else if(node.data('nivel') == 3){
            node.style('shape', 'diamond');
            node.style("background-color","#0000FF");
            node.data('type', 'Implication');
          }
        })
      }
    })
  })
        


$('#saveMapbtn').on('click', function(){
  $.ajax({
    type: 'POST',
    url: '/maps',
    data: {
      title: window.location.pathname.substr(1),
      content: JSON.stringify(cy.json())
    },
    success: function(response) {
      console.log(response);
    },
    error: function(){
      console.log('error posting map');
    }
  });
})

  var current = {
    color: 'black'
  };

  var dragging = false;

  var mapTitle = window.location.pathname.substr(1);


  //chamo as funções do socket via os eventos abaixo. Eles são chamados quando executamos o socket.emit
  socket.on('connect', function(){
    socket.emit('storeName', {mapTitle: mapTitle})
  })
  socket.on('adding', onAddingEvent);
  socket.on('removing', onRemovingEvent);
  socket.on('dragging', onDraggingEvent);
  socket.on('renaming', onRenamingEvent);
  socket.on('addingCompundAlternative', onAltEvent);
  socket.on('addingCharacterizer', onCharcterizingEvent);
  socket.on('updatingDescription', onUpdatingDescriptionEvent);
  socket.on('editingChar', onEditingCharEvent);

  
      
var cy = window.cy = cytoscape({
          container: document.getElementById('cy'),
          boxSelectionEnabled: false,
          autounselectify: true,
           wheelSensitivity: 0.1,
           fit: false,
           zoom: 3,

          layout: {
            name: 'cose-bilkent'
          },

           style: [
                  {
                    selector: 'node',
                    style: {
                      'shape': 'circle',
                      'content': 'data(idNome)',
                      'text-opacity': 0.5,
                      'text-valign': 'center',
                      'text-halign': 'right',
                      'background-color': [169,169,169]
                    }
                  },
                  {
                    selector: 'edge',
                    style: {
                      'width': 1,
                      'target-arrow-shape': 'triangle',
                      'line-color': [157,186,234],
                      'target-arrow-color': [157,186,234],
                      'curve-style': 'bezier'
                    }
                  },

            {
              selector: ':parent',
              style: {
                'background-opacity': 0.333
              }
            },

            {
              selector: "node.cy-expand-collapse-collapsed-node",
              style: {
                "background-color": "darkblue",
                "shape": "rectangle"
              }
            },


            {
              selector: 'edge.meta',
              style: {
                'width': 2,
                'line-color': 'red'
              }
            },
          ],

      
 elements: {
            nodes: [
              { data: { id: '0' , nivel: 0, description: '', type: '', metric: 0},  
          "group":"nodes",
          "removed":false,
          "selected":false,
          "selectable":true,
          "locked":false,
          "grabbable":true,
          "classes":"" 
      },
            ],
            edges: []
          }, 
          fit:false,
          zoom: 3
        }
);


          // you can use qtip's regular options
          // see http://qtip2.com/
          cy.$('#0').qtip({
            id: 'Tooltip0',
            content: 'Decision' ,
            position: {
              my: 'top center',
              at: 'bottom center'
            },
            style: {
              classes: 'qtip-bootstrap',
              tip: {
                width: 16,
                height: 8
              }
            }
          });


    $(document).keydown(function(e){
      if(e.which == 18) pressedShift = true;
      if((e.which == 65 || e.keyCode == 65) && pressedShift == true){
        preAdd2();
        pressedShift = false;
      }
    });

    $(document).keydown(function(e){
      if(e.which == 18) pressedShift = true;
      if((e.which == 67 || e.keyCode == 67) && pressedShift == true){
        if (cy.getElementById(selectedNode).data("id").substring(0,3) != 'Alt'){
          alert("You can create characterizer only in Compound Alternative Nodes!");
          pressedShift = false;
        }
        else{
          $('#createCharacterizer').modal('show');
          pressedShift = false;
        }
      }
    });

    $(document).keydown(function(e){
      if(e.which == 46 || e.keyCode == 46){
        var idAtual = cy.getElementById(selectedNode).data("id");
        var temFilho = 0;
        if(cy.getElementById(selectedNode).data("nivel") == 0){
          //alert("Você não pode remover a raiz!");
          alert("Unable to remove parents!");
        }
        else{
          removeNode(selectedNode, idAtual, temFilho, true);
        }
      }
    })

          var options = {
              menuItems: [                
                    {
                      id: 'add-node',
                      content: 'Add node',
                      tooltipText: 'Add Node',
                      selector: 'node',
                      onClickFunction: function (event) {
                        if (cy.getElementById(selectedNode).data('nivel') == 3){
                          alert('There is no more possibles nodes to add from this!');
                          return;
                        }
                        $('#createNodeModal').modal('show');
                      }
                    },
                     {
                      id: 'add-compound',
                      content: 'Add Compound Alternative',
                      tooltipText: 'Add Compound Alternative',
                      selector: 'node',
                      onClickFunction: function (event) {
                        preAdd2();
                      }
                    },
                     {
                      id: 'add-characterizer',
                      content: 'Add Characterizer',
                      tooltipText: 'Add Characterizer',
                      selector: 'node',
                      onClickFunction: function (event) {
                         $('#createCharacterizer').modal('show');
                      }
                    },
                    {
                      id: 'edit-characterizer',
                      content: 'Edit Characterizer',
                      tooltipText: 'Edit Characterizer',
                      selector: 'node',
                      onClickFunction: function (event) {
                         $('#editCharacterizer').modal('show');
                         $('#CharNamesEdit').val(cy.getElementById(selectedNode).data('idNome'));
                         $('input[name="DescValEdit"]').removeAttr('checked');
                         $("input[name=DescValEdit][value=" + cy.getElementById(selectedNode).data('impacto') + "]").prop('checked', true);;

                      }
                    },
                    {
                      id: 'rename-node',
                      content: 'Rename node',
                      tooltipText: 'Rename Node',
                      selector: 'node',
                      onClickFunction: function (event) {
                        $('#renameNodeModal').modal('show');
                      }
                    },
                    {
                      id: 'remove-node',
                      content: 'Remove node',
                      tooltipText: 'Remove Node',
                      selector: 'node',
                      onClickFunction: function (event) {
                       var idAtual = cy.getElementById(selectedNode).data("id");
                       var temFilho = 0;
                        if(cy.getElementById(selectedNode).data("nivel") == 0){
                          //alert("Você não pode remover a raiz!");
                          alert("Unable to remove parents!");
                        }
                        else{
                          removeNode(selectedNode, idAtual, temFilho, true);
                        }
                      }
                    }
                    

      
                  ]
            };


            var instance = cy.contextMenus( options );
            instance.hideMenuItem("add-compound"); 
            instance.hideMenuItem("add-characterizer"); 
            instance.hideMenuItem("edit-characterizer"); 
        cy.maxZoom(9999999);



    
            //declaro um objeto vazio para armazenar as posições do nó selecionado
            var posicao = {

            };

            $('#saveDescription').on('click', function(){
              cy.getElementById(selectedNode).data('description', $('#textArea1').val());
            });


            cy.on('click','node', function(e){

                  $('#textArea1').prop('disabled', false);
                  $('.inputMessage').blur();
                  $('.usernameInput').blur();
                  selectedNode = e.cyTarget.id();
                  cy.nodes().style('border-width', 'none');
                  cy.nodes().style('border-color', 'white');
                  cy.getElementById(selectedNode).style('border-width', '1');
                  cy.getElementById(selectedNode).style('border-color', 'black');
                  $('#textArea1').val(cy.getElementById(selectedNode).data('description'));



                  if(selectedNode.substring(0, 3) == 'Alt'){

                      array = [];
                        var marksData2 = {
                            labels: [],
                            datasets: [{
                              label: cy.getElementById(cy.getElementById(selectedNode).data('parent')).data('idNome'),
                              backgroundColor: "transparent",
                              borderColor: "rgba(200,0,0,0.6)",
                              fill: false,
                              radius: 4,
                              pointRadius: 4,
                              pointBorderWidth: 2,
                              pointBackgroundColor: "orange",
                              pointBorderColor: "rgba(200,0,0,0.6)",
                              pointHoverRadius: 10,
                              data: []
                            }]
                        };


                         console.log(cy.edges('edge[source="' + selectedNode + '"]'));
                         x = cy.edges('edge[source="' + selectedNode + '"]');
                         y = 1;
                         z = x.targets();
                         length = z.length;

                         for (var w = 0 ; w < length; w++){
                          json1 = z[w].data("idNome");
                          json2 = z[w].data("impacto");
                          array.push(json1, json2);
                         }

                        
                            radarChart.destroy();
                            radarChart = new Chart(marksCanvas, {
                              type: 'radar',
                              data: marksData2
                            });

                            for (var k = 0; k < array.length; k=k+2){
                                 radarChart.data.labels.push(array[k]);
                                 radarChart.data.datasets.forEach(function (dataset){
                                      dataset.data.push(array[k+1]);
                                });
                            }
                            window.radarChart.update();
                            alternativaAtual = selectedNode;

                  }

                  if (cy.getElementById(selectedNode).data("level") == 2){
                      instance.showMenuItem("add-node"); 
                       $( ".alternativeExclusive3" ).css('display', 'block');

                      if  (cy.getElementById(selectedNode).data("HasCompound") == "Yes"){
                        instance.hideMenuItem("add-compound"); 
                        $( ".alternativeExclusive1" ).css('display', 'none');
                      }
                      else{
                         instance.showMenuItem("add-compound"); 
                        $( ".alternativeExclusive1" ).css('display', 'block');
                      }

                      instance.hideMenuItem("add-characterizer"); 
                      $( ".alternativeExclusive2" ).css('display', 'none');
                  }
                  else if (selectedNode.substring(0, 3) == "Alt"){ 
                      instance.showMenuItem("add-characterizer"); 
                      instance.hideMenuItem("add-compound"); 
                      instance.hideMenuItem("add-node");
                      instance.hideMenuItem("edit-characterizer");
                     $( ".alternativeExclusive1" ).css('display', 'none');
                     $( ".alternativeExclusive2" ).css('display', 'block');
                     $( ".alternativeExclusive3" ).css('display', 'none');
                  }
                  else if (selectedNode.substring(0,3) =="Car"){
                    instance.hideMenuItem("add-compound"); 
                    instance.hideMenuItem("add-characterizer"); 
                    instance.hideMenuItem("add-node");
                    instance.showMenuItem("edit-characterizer"); 
                     $( ".alternativeExclusive1" ).css('display', 'none');
                     $( ".alternativeExclusive2" ).css('display', 'none');
                     $( ".alternativeExclusive3" ).css('display', 'none');
                  }
                  else{
                    instance.showMenuItem("add-node");
                    instance.hideMenuItem("add-compound"); 
                    instance.hideMenuItem("add-characterizer"); 
                    instance.hideMenuItem("edit-characterizer");
                    $( ".alternativeExclusive1" ).css('display', 'none');
                    $( ".alternativeExclusive2" ).css('display', 'none');
                    $( ".alternativeExclusive3" ).css('display', 'block');
                  }
              });

            cy.on('mousedown','node', function(e){
              //troco o valor dessa variavel para permitir a emissão do evento
                dragging = true;
                $('.inputMessage').blur();
                $('.usernameInput').blur();
                selectedNode = e.cyTarget.id();
                  posicao.x  = e.cyRenderedPosition.x;
                  posicao.y = e.cyRenderedPosition.y;
              });

              cy.on('mouseup','node', function(e){
                  if (!dragging) { return; }
                    dragging = false;
                    selectedNode = e.cyTarget.id();
                    //quando solto o botão do mouse, mando atualizar a posição do nó 
                   updatePosition(e.cyPosition.x, e.cyPosition.y, selectedNode, true);
              });


    
function updatePosition(x0, y0, selectedNode, emit){
 
    cy.$('#' + selectedNode).position({
      x: x0,
      y: y0
    });

    //essa validação do emit tem uma tirada importante. Quando chamamos no onDraggingEvent, o valor
    //emit não é enviado. Ele então é dado como undefined e nega a emissão. Isso evita o loop que
    //iria acontecer quando essa função chega no cliente. O mesmo só atualiza o valor e para de emitir.
     if (!emit) { return; }
        var w = cy.width();
        var h = cy.height();


    //Emito os valores que eu quero para a função desejada
        socket.emit('dragging', {
            x0: x0 / w,   //divido por w e h só para ter o valor certo do viewport atual
            y0: y0 / h,
            selectedNode: selectedNode
         });
}

  // limit the number of events per second
  function throttle(callback, delay) {
    var previousCall = new Date().getTime();
    return function() {
      var time = new Date().getTime();

      if ((time - previousCall) >= delay) {
        previousCall = time;
        callback.apply(null, arguments);
      }
    };
  }

  function onDraggingEvent(data){
      var w = cy.width();
      var h = cy.height();
      updatePosition(data.x0 * w, data.y0 * h, data.selectedNode); 
      //chamo a função novamente para gerá-los no cliente

      // cy.getElementById(data.selectedNode).position(data.x0 * w, data.y0 * h);
      // cy.getElementById(selectedNode).position(x0,y0);

      socket.emit('autosave', { 
        title: window.location.pathname.substr(1),
        content: JSON.stringify(cy.json())
        });
  }

  function onAddingEvent(nodedata){
    console.log("evento");
    addNode(nodedata.selectedNode, nodedata.idText, nodedata.nivelPai, nodedata.nodeDescription);
  }

  function onAltEvent(altdata){
    console.log("Compund Alternative");
    addNode2(altdata.selectedNode, altdata.idText, altdata.nivelPai);
  }

  function onCharcterizingEvent(chardata){
    console.log("Characterizer");
    addNode3(chardata.selectedNode, chardata.idText, chardata.nivelPai, chardata.CarImpact);
  }

  function onRemovingEvent(nodeData){
    console.log("Removing");
    removeNode(nodeData.selectedNode, nodeData.idAtual, nodeData.temFilho);
  }
  function onRenamingEvent(nodeDataRename){
    console.log("Renaming");
    renameNode(nodeDataRename.selectedNode, nodeDataRename.idText);
  }
  function onEditingCharEvent(nodeCharEdit){
    console.log("EditingChar");
    editChar(nodeCharEdit.selectedNode, nodeCharEdit.idText, nodeCharEdit.CarImpact);
  }
  function onUpdatingDescriptionEvent(nodeUpdateEdit){
    console.log("UpdatingDescription");
    updateDescription(nodeUpdateEdit.selectedNode, nodeUpdateEdit.nodeDescription);
  }

  function updateDescription(selectedNode, newDescripton){
    cy.getElementById(selectedNode).data('description', newDescripton);
    console.log('teste description');
  }



function addNode(selectedNode, idText, nivelPai , nodeDescription, emit){
    nId = 0;
    var aux = 0;
    for (i = 0; i < cy.nodes().length ; i++){
      aux = cy.nodes()[i].data().id;
     if (aux.substr(0,1) === "A" || aux.substr(0,1) === "C"){
       console.log("Alt composta ou caracterizador");
     }
     else{
       if (cy.nodes()[i].data().id > nId){
         nId = cy.nodes()[i].data().id;
         console.log("new nId: " + nId);
       }
     }
    }
    nId = parseInt(nId);
    cy.add([
          { group: "nodes", data: {id:  nId+1, idNome:idText, level: nivelPai + 1}, position: {x: cy.getElementById(selectedNode).position("x")+50, y: cy.getElementById(selectedNode).position("y")+50}},
          { group: "edges", data: { id: 'edge'+nId, source: selectedNode, target: nId+1 }}
    ]);
    cy.getElementById(nId+1).data("nivel", nivelPai + 1);
    cy.getElementById(nId+1).data("description", nodeDescription);
    nId++;
    var textTooltip = null;


    var nodeLevel = cy.getElementById(nId).data("nivel");
    
        if(nodeLevel == 1){
          cy.getElementById(nId).style('shape', 'triangle');
          cy.getElementById(nId).style("background-color","#00FF00");
          cy.getElementById(nId).data('type', 'Scenario');
          textTooltip = "Scenario ";
          ScenarioCount++;
          NumberNode = ScenarioCount;
        }
        else if(nodeLevel == 2){
          cy.getElementById(nId).style('shape', 'roundrectangle');
          cy.getElementById(nId).style("background-color","#FF8C00"); 
          cy.getElementById(nId).data('type', 'Alternative');
          textTooltip = "Alternative ";
          AlternativeCount++;
          NumberNode = AlternativeCount;
        }
        else if(nodeLevel == 3){
          cy.getElementById(nId).style('shape', 'diamond');
          cy.getElementById(nId).style("background-color","#0000FF");
          cy.getElementById(nId).data('type', 'Implication');
          textTooltip = "Implication ";
          ImplicationCount++;
          NumberNode = ImplicationCount;
        }
        else if(nodeLevel == 4){
          cy.getElementById(nId).style('shape', 'star');
          cy.getElementById(nId).style("background-color","#FF0000");
          cy.getElementById(nId).data('type', 'Impact');
          textTooltip = "Impact ";
          ImpactCount++;
          NumberNode = ImpactCount;

        }


         cy.$('#'+ nId).qtip({
            id: 'Tooltip' +varTooltip+'',
            content: nodeDescription ,
            position: {
              my: 'top center',
              at: 'bottom center'
            },
            style: {
              classes: 'qtip-bootstrap',
              tip: {
                width: 16,
                height: 8
              }
            }
          });


    if (!emit) { return; }

        socket.emit('adding', {
            selectedNode: selectedNode,
            idText: idText,
            nivelPai: nivelPai,
            nodeDescription: nodeDescription,
            // i: i
        });    

    varTooltip = varTooltip + 1;

    socket.emit('autosave', { 
      title: window.location.pathname.substr(1),
      content: JSON.stringify(cy.json())
      });

}


function addNode2(selectedNode, idText, nivelPai , emit){ //ivalue
    nAlt++;
    cy.add([
          { group: "nodes", data: {id: "Alt"+nAlt, level: nivelPai + 1 , parent: selectedNode}, position: {x: cy.getElementById(selectedNode).position("x")+50, y: cy.getElementById(selectedNode).position("y")+50}}
    ]);
    cy.getElementById("Alt"+nAlt).data("nivel", nivelPai);
    cy.getElementById("Alt"+nAlt).data("idNome", idText);
    
    cy.getElementById(selectedNode).data("HasCompound", "Yes");

    var nodeLevel = cy.getElementById("Alt"+nAlt).data("nivel");

          cy.getElementById("Alt"+nAlt).style('shape', 'roundrectangle');
          cy.getElementById("Alt"+nAlt).style("background-color","#FF8C00"); 
          cy.getElementById("Alt"+nAlt).data('type', 'Alternative');

    if (!emit) { return; }

        socket.emit('addingCompundAlternative', {
            selectedNode: selectedNode,
            idText: idText,
            nivelPai: nivelPai
        });

        socket.emit('autosave', { 
          title: window.location.pathname.substr(1),
          content: JSON.stringify(cy.json())
          });
}

function addNode3(selectedNode, idText, nivelPai, CarImpact, emit ){ //ivalue

  altAtual = cy.getElementById(selectedNode).data("parent");

    nCar++;
    cy.add([
          { group: "nodes", data: {id: "Car"+ nCar, idNome:idText, level: nivelPai + 1, parent: altAtual, impacto: CarImpact }, position: {x: cy.getElementById(selectedNode).position("x")+50, y: cy.getElementById(selectedNode).position("y")+50}},
          { group: "edges", data: {id: 'edgeCar'+nCar, source: selectedNode, target: "Car"+ nCar }}
    ]);

          cy.getElementById("Car"+ nCar).style('shape', 'roundrectangle');
          cy.getElementById("Car"+ nCar).style("background-color",[169,169,169]); 
          cy.getElementById("Car"+ nCar).data('type', 'Alternative');


    if (!emit) { return; }

        socket.emit('addingCharacterizer', {
            selectedNode: selectedNode,
            idText: idText,
            nivelPai: nivelPai,
            CarImpact: CarImpact
      });     

        var descriptionNode =  cy.getElementById(nId).data('description');



    cy.$("#Car1").data("impacto");
    cy.$("#1").data("level");

    socket.emit('autosave', { 
      title: window.location.pathname.substr(1),
      content: JSON.stringify(cy.json())
      });

}




function removeNode(selectedNode, idAtual, temFilho, emit){
    let saveNode = selectedNode;
    let passow = false;
    cy.edges().forEach(function (edge){
      if(edge.source().id() == selectedNode){
        alert("You can only remove leaves!");
        temFilho=1;
        return;
      }
    });
    cy.edges().forEach(function (edge){
    if(edge.target().id() == selectedNode){
      selectedNode = edge.source().id();
      passow = true;
    }
  });
    if(temFilho==0){
      cy.getElementById(saveNode).remove();
    }
    
    if (!passow){
      if (!(cy.nodes().length == 0)){
        selectedNode = cy.nodes()[0].id();
      }
    }

    if (!emit) { return; }

        socket.emit('removing', {
            selectedNode: saveNode,
            idAtual: idAtual,
            temFilho: temFilho,
            // i: i
        });


        socket.emit('autosave', { 
          title: window.location.pathname.substr(1),
          content: JSON.stringify(cy.json())
          });
}



function renameNode(selectedNode, idText, emit){  
  cy.getElementById(selectedNode).data("idNome", idText);
  $('#renameNodeModal').modal('hide');
  $('#renameNodeModal').find('.modal-body input').val("")

  if (!emit) { return; }

      socket.emit('renaming', {
          selectedNode: selectedNode,
          idText: idText,
      });

      socket.emit('autosave', { 
        title: window.location.pathname.substr(1),
        content: JSON.stringify(cy.json())
        });
}

function editChar(selectedNode, idText, CarImpact, emit){  

  cy.getElementById(selectedNode).data("impacto", CarImpact);
  cy.getElementById(selectedNode).data("idNome", idText);
  console.log('teste');
  $('#editCharacterizer').modal('hide');


  if (!emit) { return; }

      socket.emit('editingChar', {
          selectedNode: selectedNode,
          idText: idText,
          CarImpact: CarImpact,
      });

      socket.emit('autosave', { 
        title: window.location.pathname.substr(1),
        content: JSON.stringify(cy.json())
        });
}


function preAdd2(){
  let idText = $("#nodeNames").val();
      let nivelPai = cy.getElementById(selectedNode).data("nivel");

      if (nivelPai == 2){
          var nodeLevel = cy.getElementById(nId).data("nivel");

          if (!(cy.nodes().length == 0)){
           addNode2(selectedNode, idText, nivelPai, true);
          }
      }
}

function preAdd3(){
  let idText = $("#CharNames").val();
  let CarImpact =  $('input[name=DescVal]:checked', '.CharacterizerValue').val();
    let nivelPai = cy.getElementById(selectedNode).data("nivel");
    var nodeLevel = cy.getElementById(nId).data("nivel");
      if (idText == ''){
        alert("Write the name of the Characterizer!");
      }
      else if (CarImpact == undefined){
        alert("Give a value to the Characterizer!");
      }
      else if (nivelPai != 2){
        alert("You can create characterizer only in Alternative Nodes!");
      }
      else{
        if (!(cy.nodes().length == 0)){
         addNode3(selectedNode, idText, nivelPai,CarImpact, true);
        }
      $('#createCharacterizer').modal('hide');
      $('#CharNames').val("");
      $('input[name="DescVal"]').removeAttr('checked');
      }
      radarChart.data.labels.push(idText);
      radarChart.data.datasets.forEach(function (dataset){
          dataset.data.push(CarImpact);
      });
      window.radarChart.update();
}



$('#radarChartOption').on('click', function () {
  $('#radarChartModal').modal('show');
});

$('#url2').on('click', function () {
    alert("haha");
    var url=radarChart.toBase64Image();

    var dd = {
    content: [
      {
        text: 'Radar Chart\n\n\n',
        style: 'header'
      },
      {
      image: 'chart',
      width: 500
    },
  ],
  images: {
    chart: url
  },
}

  pdfMake.createPdf(dd).open();
});




$('#add').on('click', function () {
        let idText = $("#nodeNames").val();
        let nivelPai = cy.getElementById(selectedNode).data("nivel");
        var nodeDescription = $('#desc').val();

          if (idText == ''){
            alert("Write the name of the node!");
          }
          else if (nodeDescription == ""){
            alert("Write the description of the node!");
          }
          else{
            if (!(cy.nodes().length == 0)){
             addNode(selectedNode, idText, nivelPai, nodeDescription, true);
            }
          $('#createNodeModal').modal('hide');
          $('#createNodeModal').find('.modal-body input').val("")
          $('#desc').val('');
          }
 });


    $('#update').on('click', function (){
            let nodeDescription = $("#recipient-name").val();
            updateDescription(selectedNode, nodeDescription, true);
            $('#descriptionModal').modal('hide');

            socket.emit('autosave', { 
              title: window.location.pathname.substr(1),
              content: JSON.stringify(cy.json())
              });
    });


$('#closeCreateModal').on('click', function(){
     $('#createNodeModal').modal('hide');
     $('#createNodeModal').find('.modal-body input').val("")
     $('#desc').val('');
});



$('#addFromMenu').on('click', function(){
  if (cy.getElementById(selectedNode).data('nivel') == 3){
    alert('There is no more possibles nodes to add from this!');
    return;
  }
  else{
    $('#createNodeModal').modal('show');
    $('#createNodeModal').find('.modal-body input').val("")
  }
});

$('#add2').on('click', function () {
      preAdd2();
  });

$('#add3').on('click', function () {
  if (cy.getElementById(selectedNode).data("id").substring(0,3) != 'Alt'){
    alert("You can create characterizer only in Compound Alternative Nodes!");
  }
  else{
    preAdd3();
  }

});


$('#remove').on('click', function () {
      var idAtual = cy.getElementById(selectedNode).data("id");
      var temFilho = 0;
      if(cy.getElementById(selectedNode).data("nivel") == 0){
        //alert("Você não pode remover a raiz!");
        alert("Unable to remove parents!");
      }
      else{
        removeNode(selectedNode, idAtual, temFilho, true);
      }
});

  


$("#edit").on ("click",function (){
  let idText = $("#nodeRename").val();
  renameNode(selectedNode, idText, true);      
});

$("#updateChar").on("click",function(){

  let idText = $("#CharNamesEdit").val();
  let CarImpact =  $('input[name=DescValEdit]:checked', '.CharacterizerValue').val();

   if (idText == ''){
        alert("Write the name of the Characterizer!");
    }
    else if (CarImpact == undefined){
        alert("Give a value to the Characterizer!");
    }
    else{
      editChar(selectedNode, idText, CarImpact, true);
    }
});


$("#exportChat").on("click", function(){
  var dd = {
    content: [
      {
        text: 'Chat Log\n\n\n',
        style: 'header'
      },
    ],
    styles: {
      header: {
        fontSize: 20,
        bold: true
      },
      username: {
        fontSize: 10,
        bold: true
      },
      message: {
        fontSize: 10,
        margin: [10, 0, 0, 0]
      },
      hour:{
        fontSize: 8,
        margin: [350, 0, 0, 0]
      }
    }
  }
  var size = dd.content.length;
  chatMessages.forEach(function(cm){
    dd.content[size] = [
      {
        text: cm.username + ":",
        style: 'username'
      },
      {
        text: cm.message,
        style: 'message'          
      },
      {
        text: cm.hour + "\n",
        style: 'hour'
      }
    ];
    size = dd.content.length;
  });

  

  pdfMake.createPdf(dd).open();
});


var ur = cy.undoRedo(); // external (optional) extension which enables undo/redo of expand/collapse



        cy.expandCollapse({
          layoutBy: {
            name: "cose-bilkent",
            animate: "end",
            randomize: false,
            fit: true
          },
          fisheye: true,
          animate: true
        });


  var FADE_TIME = 150; // ms
  var TYPING_TIMER_LENGTH = 400; // ms
  var COLORS = [
    '#e21400', '#91580f', '#f8a700', '#f78b00',
    '#58dc00', '#287b00', '#a8f07a', '#4ae8c4',
    '#3b88eb', '#3824aa', '#a700ff', '#d300e7'
  ];

  // Initialize variables
  var $window = $(window);
  var $usernameInput = $('.usernameInput'); // Input for username
  var $messages = $('.messages'); // Messages area
  var $inputMessage = $('.inputMessage'); // Input message input box

  var $loginPage = $('.login.page'); // The login page
  var $chatPage = $('.chat.page'); // The chatroom page

  // Prompt for setting a username
  var username;
  var connected = false;
  var typing = false;
  var lastTypingTime;

  // var $currentInput = $usernameInput.focus();

  // var socket = io();

  function addParticipantsMessage (data) {

    var message = '';


    if (data.numUsers === 1) {
      message += "there's 1 participant";
    } else {
      message += "there are " + data.numUsers + " participants ";
    }
    log(message);
  }


  // Sets the client's username
  function setUsername () {
    // username = getCookie("Name");
    username = cleanInput($usernameInput.val().trim());
    // If the username is valid
    if (username) {
      $loginPage.fadeOut();
      $chatPage.show();
      $loginPage.off('click');
      // $currentInput = $inputMessage.focus();

      // Tell the server your username
      socket.emit('add user', username);
    }
  }
  //setUsername();

  // Sends a chat message
  function sendMessage () {
    var message = $inputMessage.val();
    // Prevent markup from being injected into the message
    message = cleanInput(message);
    // if there is a non-empty message and a socket connection
    if (message && connected) {
      $inputMessage.val('');
      addChatMessage({
        username: username,
        message: message
      });
      // tell server to execute 'new message' and send along one parameter
      socket.emit('new message', message);
    }
  }

  function getCookie(cname) { //tive que repetir essa função pq não consegui acessar o escopo de jeito nenhum
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

   function sendVideoConfMessage () {
    var chatid = getCookie("VideoURL");
    var message = "I just started a videoconference about this map! Open this link in a new tab and join us : http://localhost:8000/video-conferencing/index.html" + chatid + "";
    // Prevent markup from being injected into the message
    message = cleanInput(message);
    // if there is a non-empty message and a socket connection
    if (message && connected) {
      $inputMessage.val('');
      addChatMessage({
        username: username,
        message: message
      });
      // tell server to execute 'new message' and send along one parameter
      socket.emit('new message', message);
    }
  }


  $("#ShareLink").click(function() {
    sendVideoConfMessage();
  });





  // Log a message
  function log (message, options) {
    var $el = $('<li>').addClass('log').text(message);
    addMessageElement($el, options);
  }

  // Adds the visual chat message to the message list
  function addChatMessage (data, options) {
    // Don't fade the message in if there is an 'X was typing'
    var $typingMessages = getTypingMessages(data);
    options = options || {};
    if ($typingMessages.length !== 0) {
      options.fade = false;
      $typingMessages.remove();
    }

  
   // var $userImg = $('<img src="'+ImageURL+'"; title="Nome: '+ GoogleName+ '\nEmail: ' + emailGoogle +' " ; style=" height:22px; width: 22px; border-radius: 50%;  position: relative; top: 25%; left: -5% ; margin-bottom: 5px;">');
    var $usernameDiv = $('<span class="username"/>')
      .text(data.username +" ")
      .css('color', getUsernameColor(data.username));
    var $messageBodyDiv = $('<span class="messageBody">')
      .text(data.message + " " )
    var $timestampDiv = $('<span class="timeStamp">')
        .text(moment().format('h:mm a'))
        .css('color', '#C0C0C0')
        .css('float', 'right');
    var typingClass = data.typing ? 'typing' : '';
    var $messageDiv = $('<li class="message"/>')
      .data('username', data.username)
      .addClass(typingClass)
      .append( $usernameDiv, $messageBodyDiv, $timestampDiv);

    addMessageElement($messageDiv, options);

    chatMessages.push({username: $messageDiv.data('username'), message: $messageBodyDiv.text(), hour: moment().format('MMMM Do YYYY, h:mm a')});

    if(chatStatus == 0){
        if($("#page").hasClass("page-toggle")){
          var sound = document.getElementById("audio");
          sound.play();
          $("#blinking").addClass('blinking');
        }
      }
      chatStatus = 0;
  }

  // Adds the visual chat typing message
  function addChatTyping (data) {
    data.typing = true;
    data.message = 'is typing';
    addChatMessage(data);
  }

  // Removes the visual chat typing message
  function removeChatTyping (data) {
    getTypingMessages(data).fadeOut(function () {
      $(this).remove();
    });
  }

  // Adds a message element to the messages and scrolls to the bottom
  // el - The element to add as a message
  // options.fade - If the element should fade-in (default = true)
  // options.prepend - If the element should prepend
  //   all other messages (default = false)
  function addMessageElement (el, options) {
    var $el = $(el);

    // Setup default options
    if (!options) {
      options = {};
    }
    if (typeof options.fade === 'undefined') {
      options.fade = true;
    }
    if (typeof options.prepend === 'undefined') {
      options.prepend = false;
    }

    // Apply options
    if (options.fade) {
      $el.hide().fadeIn(FADE_TIME);
    }
    if (options.prepend) {
      $messages.prepend($el);
    } else {
      $messages.append($el);
    }
    $messages[0].scrollTop = $messages[0].scrollHeight;
  }

  // Prevents input from having injected markup
  function cleanInput (input) {
    return $('<div/>').text(input).html();
  }

  // Updates the typing event
  function updateTyping () {
    if (connected) {
      if (!typing) {
        typing = true;
        socket.emit('typing');
      }
      lastTypingTime = (new Date()).getTime();

      setTimeout(function () {
        var typingTimer = (new Date()).getTime();
        var timeDiff = typingTimer - lastTypingTime;
        if (timeDiff >= TYPING_TIMER_LENGTH && typing) {
          socket.emit('stop typing');
          typing = false;
        }
      }, TYPING_TIMER_LENGTH);
    }
  }

  // Gets the 'X is typing' messages of a user
  function getTypingMessages (data) {
    return $('.typing.message').filter(function (i) {
      return $(this).data('username') === data.username;
    });
  }

  // Gets the color of a username through our hash function
  function getUsernameColor (username) {
    // Compute hash code
    var hash = 7;
    for (var i = 0; i < username.length; i++) {
       hash = username.charCodeAt(i) + (hash << 5) - hash;
    }
    // Calculate color
    var index = Math.abs(hash % COLORS.length);
    return COLORS[index];
  }

  // Keyboard events

  $window.keydown(function (event) {
    // Auto-focus the current input when a key is typed
    if (!(event.ctrlKey || event.metaKey || event.altKey)) {
      // $currentInput.focus();
    }
    // When the client hits ENTER on their keyboard
    if (event.which === 13) {
      if (username) {
        chatStatus = 1;
        sendMessage();
        socket.emit('stop typing');
        typing = false;
      } else {
        setUsername();
      }
    }
  });

  $inputMessage.on('input', function() {
    updateTyping();
  });



  // Click events

  // Focus input when clicking anywhere on login page
  // $loginPage.click(function () {
  //   $currentInput.focus();
  // });

  // Focus input when clicking on the message input's border
  // $inputMessage.click(function () {
  //   $inputMessage.focus();
  // });

  // Socket events

  // Whenever the server emits 'login', log the login message
  socket.on('login', function (data) {
    connected = true;
     // addData(data.username, 1);
    // Display the welcome message
    var message = "Welcome to IMAP Chat – ";
    log(message, {
      prepend: true
    });
    addParticipantsMessage(data);
  });

  // Whenever the server emits 'new message', update the chat body
  socket.on('new message', function (data) {
    addChatMessage(data);
  });

  // Whenever the server emits 'user joined', log it in the chat body
  socket.on('user joined', function (data) {
    log(data.username + ' joined');
    addParticipantsMessage(data);
  });

  // Whenever the server emits 'user left', log it in the chat body
  socket.on('user left', function (data) {
    log(data.username + ' left');
    addParticipantsMessage(data);
    removeChatTyping(data);
  });

  // Whenever the server emits 'typing', show the typing message
  socket.on('typing', function (data) {
    addChatTyping(data);
  });

  // Whenever the server emits 'stop typing', kill the typing message
  socket.on('stop typing', function (data) {
    removeChatTyping(data);
  });

  socket.on('disconnect', function () {
    log('you have been disconnected');
  });

  socket.on('reconnect', function () {
    log('you have been reconnected');
    if (username) {
      socket.emit('add user', username);
    }
  });

  socket.on('reconnect_error', function () {
    log('attempt to reconnect has failed');
  });

})();


