(function() {
  $(function() {
    $('body').chardinJs();
    $('#tutorial').on('click', function(e) {
      e.preventDefault();
      cy.zoom(3);
        cy.center();
        return ($('body').data('chardinJs')).toggle();
    });
  });
}).call(this);
