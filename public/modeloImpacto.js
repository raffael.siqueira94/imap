
    var pressedShift = false;
    var checked;
   
     var alt = $(window).height() - $(".navbar").height() - $(".footer").height();
     $("#cy").css("height", alt);

     $( window ).resize(function() {
        var alt = $(window).height() - $(".navbar").height() - $(".footer").height();
        $("#cy").css("height", alt);
    });


      $("#menu-toggle").click(function(e) {
       e.preventDefault();
       $("#wrapper").toggleClass("active");
       $("#cy").toggleClass("active");
      });


    $('input').keypress(function (ev) {
        if (ev.which == 13) {
            ev.preventDefault();
            $('#' + $(this).prop('name')).trigger('click');
        }
    });

    $('button').click(function (ev) {
        ev.preventDefault();
    });
    
    
    $(document).keydown(function(e){
      if(e.which == 18) pressedShift = true;
      if((e.which == 78 || e.keyCode == 78) && pressedShift == true) {
        if (cy.getElementById(selectedNode).data('nivel') == 3){
          alert('There is no more possibles nodes to add from this!');
          return;
        }
        $('#createNodeModal').modal('show');
        pressedShift = false;
      }
    })

    $(document).keydown(function(e){
      if(e.which == 18) pressedShift = true;
      if((e.which == 82 || e.keyCode == 82) && pressedShift == true) {
        $('#renameNodeModal').modal('show');
        pressedShift = false;
      }
    })
    
    $(document).keydown(function(e){
      if(e.which == 18) pressedShift = true;
      if((e.which == 77 || e.keyCode == 77) && pressedShift == true) {
        center();
        pressedShift = false;
      }
    })
    $(document).keydown(function(e){
      if(e.which == 18) pressedShift = true;
      if((e.which == 68 || e.keyCode == 68) && pressedShift == true) {
        $('#descriptionModal').modal('show');
        pressedShift = false;
      }
    })

    function testeJSON(){
      var nomeMapa = prompt("Digite o nome do seu mapa");
      var textoJSON = '';
      textoJSON = JSON.stringify( cy.json() );
      location.href = "generateJSON.php?texto="+textoJSON+"&email="+emailGoogle+"&nome="+nomeMapa;
    }

    function loadMapfromJSON(){
      var aux = 0;
      var nomeMapa = prompt("Digite o nome do mapa que deseja carregar");
      //var jsonexportada;
      $.ajax({
        type: 'post',
        dataType: 'json',
        url: 'lerJSON.php',
        data: {
          nome: nomeMapa,
          email: emailGoogle
        },
        success: function(dados){
          
          cy.remove(cy.elements());
          cy.json(JSON.parse(dados.json));
          cy.nodes().forEach(function(node){
            if (node.data('nivel') == 1){
              node.style('shape', 'triangle');
            node.style("background-color","#00FF00");
            node.data('type', 'Scenario');
            }
            else if(node.data('nivel') == 2){
            node.style('shape', 'roundrectangle');
            node.style("background-color","#FF8C00"); 
            node.data('type', 'Alternative');
          }
          else if(node.data('nivel') == 3){
            node.style('shape', 'diamond');
            node.style("background-color","#0000FF");
            node.data('type', 'Implication');
          }
          });
          
        }
      });

      
    }

    function invite(){
      let email = $('#emailInput').val()
      location.href = "envia_email_gmail.php?email="+email;
    }
    
    function pdf(){
     var aux = 0;
     var typeOfNode = '';
      var dd = {
        content: [
          {
            text: 'Descriptions Report\n\n\n',
            style: 'header'
          },
          {
            text: 'Decision' + ' - ' +  cy.nodes()[0].style('content') + '\n',
            style: 'subheader'
          },
          'Description: ' + cy.nodes()[0].data('description') + '\n\n',
        ],
        styles: {
          header: {
            fontSize: 20,
            bold: true
          },
          subheader: {
            fontSize: 18,
            bold: true
          },
          subsubheader:{
            fontsize: 15,
            bold: true
          }
        }
      }
     
      while(aux < 3){
        if (aux==0){
          typeOfNode = 'Scenario';
        }
        else if (aux == 1){
          typeOfNode = 'Alternative';
        }
        else if (aux == 2){
          typeOfNode = 'Implication';
        }
        dd.content[dd.content.length] = {
          text: typeOfNode + 's:\n\n',
          style: 'subheader'
        };
        cy.nodes().forEach(function(node){
          if (node.data('id').substr(0,1) != 'A' && 
              node.data('id').substr(0,1) != 'C' && 
              node.data('type') == typeOfNode){
            var size = dd.content.length;
            dd.content[size] = {
              text: node.data('type') + ' - ' +  node.style('content') + '\n',
              style: 'subsubheader'
            };
            dd.content[size+1] = 'Description: ' + node.data('description') + '\n\n';
          } 
          
        });
        aux = aux+1;
      }

      pdfMake.createPdf(dd).open();     
     
    }
 
     
    function remove(){
      var idAtual = cy.getElementById(selectedNode).data("id");
      console.log(idAtual);
      //var arestas = cy.elements('edge[source=idAtual]');
      var arestas = cy.edges("[source="+ idAtual +"]");
      var temFilho = 0;
      if(cy.getElementById(selectedNode).data("nivel") == 0){
        alert("Unable to remove the decision!");
      }else if(arestas.length != 0){
        alert("You can only remove leaves!");
      }
      else{
        let saveNode = selectedNode;
        let passow = false;
        cy.edges().forEach(function (edge){
          if(edge.source().id() == selectedNode){
            if (temFilho != 1){
              alert("You can only remove leaves");
              temFilho=1;
            }            
            return;
          }
        });
        cy.edges().forEach(function (edge){
        if(edge.target().id() == selectedNode){
          selectedNode = edge.source().id();
          passow = true;
        }
      });
        if(temFilho==0){
          cy.getElementById(saveNode).remove();
        }
        
        if (!passow){
          if (!(cy.nodes().length == 0)){
            selectedNode = cy.nodes()[0].id();
          }
        }
        
      }

    }

    function center(){
      cy.fit();
    }

    $('#descriptionModal').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget) // Button that triggered the modal
      var recipient = button.data('whatever') // Extract info from data-* attributes
      var text = cy.getElementById(selectedNode).data("description");
      // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
      // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
      var modal = $(this)
      modal.find('.modal-title').text('Description of ' + cy.getElementById(selectedNode).data("idNome"))
      modal.find('#recipient-name').val(text)
    });

    $('#createNodeModal').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget) // Button that triggered the modal
      var recipient = button.data('whatever') // Extract info from data-* attributes
      // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
      // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
      var modal = $(this)
      modal.find('.modal-title').text('Creating a new node')
    });

    $('#renameNodeModal').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget) // Button that triggered the modal
      var recipient = button.data('whatever') // Extract info from data-* attributes
      // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
      // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
      var modal = $(this)
      modal.find('.modal-title').text('Renaming a node')
    });


$(document).ready(function(){
  $(".usernameInput").val("");

});


$("#invite").click(function() {

     var from,to,subject,text;  
        to= $("#emailInput").val();
        subject="Invite to an iMap Sesssion";
        text="Hello! I would like for you to collaborate with my map. You can access it from anytime from the link " + window.location.href  + "";

        $.get("http://localhost:8000/send",{to:to,subject:subject,text:text},function(data){
        });


});





// retorna valor armazenado no cookie e separa em categorias

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}


function centerMap(){
  $(document).ready(function(){
    cy.zoom(3);
    cy.center();
  });
}

$(window).on('load',function(){
    var value = getCookie("value");
    if (value == ""){
      $('#IntroductionModal').modal('show');

    }
    else{
      if (window.location.pathname.substr(1) === ""){
        $('#saveMapModal').modal('show');
      }
    }    
    centerMap();
});


$("#tutorial").click(function() {
    $("#img1").css("display", "block");
});

$("#tutorial2").click(function() {
    $("#img1").css("display", "block");
    $("#cyFront").css("display", "none");
    cy.zoom(3);
    cy.center();
  var checked = $('#stopAsking').is(":checked");
  if (checked == true){
    document.cookie = "value=0";
  }
    return ($('body').data('chardinJs')).toggle();
});

$(document).on('click', '.chardinjs-overlay', function () {
  if (window.location.pathname.substr(1) === ""){
    $('#saveMapModal').modal('show');
  }
    $("#img1").css("display", "none");
    $("#img2").css("display", "none");
});

$("#videoConf").click(function() {
  window.open("/video-conferencing/index.html");
  setTimeout(function(){ // o cookie pega o valor antigo se não der tempo pra ele atualizar na outra página. Tive que recorrer a isso....
    var chatid = getCookie("VideoURL");
    $("#chatid").text( "http://localhost:8000/video-conferencing/index.html" + chatid + "");
    $('#VideoConference').modal('show');
  }, 3000);
});

var cookie = document.cookie;

$("#closeTutorial").click(function() {
  checked = $('#stopAsking').is(":checked");
  if (checked == true){
    document.cookie = "value=0";
  }
  cy.zoom(3);
  cy.center();
  $("#cyFront").css("display", "none");
});


var togSrc = ["images/maps.png" ,"images/symbols-imap.png"];

$(".tog").click(function() {
  this.src = togSrc[ this.src.match('maps') ? 1 : 0 ];
});


