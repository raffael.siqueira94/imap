'use strict';
const mongoose = require('mongoose');
const Index = mongoose.model('Index');

exports.get = async() => {
    const res = await Map.find({}, 'title');
    return res;
};

exports.getByTitle = async(title) => {
    console.log('oi');
    const res = await Map.find({
        title: title,
    }, 'title content');
    return res;
};

exports.create = async(data) => {
    var map = new Map(data);
    await map.save();
};

exports.update = async(id, data) => {
    await Map.findByIdAndUpdate(id, {
        $set: {
            title: data.title,
            content: data.content
        }
    });
};

exports.delete = async(id) => {
    await Map.findByIdAndRemove(id);
}