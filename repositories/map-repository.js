'use strict';
const mongoose = require('mongoose');
const Map = require('../models/map.js');

exports.get = async() => {
    const res = await Map.find({}, 'title');
    return res;
};

exports.getByTitle = async(title) => {
    const res = await Map.find({
        title: title,
    }, 'title content');
    return res;
};

exports.create = async(data) => {
    var map = new Map(data);
    await map.save();
};

exports.update = async(data) => {
    await Map.findOneAndUpdate(
        { title: data.title },
        { content: data.content },
        function(err, doc){
            if (err){
                console.log('erro ao atualizar');
            }
            else{
                console.log('sucesso');
                console.log(doc);
            }
        }
    );
};

exports.delete = async(id) => {
    await Map.findByIdAndRemove(id);
}