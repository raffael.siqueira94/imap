
const express = require('express');
const app = express();
const router = express.Router();
const http = require('http').Server(app);
const debug = require('debug')('imap:server');
const io = require('socket.io')(http);
const port = Number(process.env.PORT || 8000);
var url = require("url");
var path = require('path');
var server = require('http').createServer(app);
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const config = require('./config');
var nodemailer = require('nodemailer');
const maprep = require('./repositories/map-repository.js');


// Variaveis para email
var smtpTransport = nodemailer.createTransport({
    service: 'gmail',
    host: 'smtp.gmail.com',
    port: 587,
    auth: {
        user: 'impact.alternative.model@gmail.com',
        pass: 'Gr3c02013'
    },
    tls: {rejectUnauthorized: false},
    debug:true
});


/*------------------SMTP Over-----------------------------*/

/*------------------Routing Started ------------------------*/

app.get('/',function(req,res){
  res.sendfile('public/index.html');
});

app.get('/send',function(req,res){
  var mailOptions={
    to : req.query.to,
    subject : req.query.subject,
    text : req.query.text
  }
  console.log(mailOptions);
  smtpTransport.sendMail(mailOptions, function(error, response){
     if(error){
          console.log(error);
    res.end("error");
   }else{
          console.log("Message sent: " + response.message);
    res.end("sent");
       }
});
});


//Google drive
/*var {google} = require("googleapis");
var drive = google.drive("v3");
var key = require("./private_key.json");
var path = require("path");
var fs = require("fs");

var jwToken = new google.auth.JWT(
  key.client_email,
  null,
  key.private_key,
  ["https://www.googleapis.com/auth/drive"],
  null
);

jwToken.authorize((authErr) => {
  if(authErr){
    console.log("erro: " + authErr);
    return;
  }
  else{
    console.log("Authorization accorded");
  }
});/*

// Listar arquivos de determinada pasta do Drive
// Esse código pode vir a ser útil na hora de fazer load do Drive

/*var folder = "176DUUo2Apuw00b3i4fh2s_Nx58Bns6yh"
drive.files.list({
  auth: jwToken,
  pagesize: 10,
  q: "'" + folder + "' in parents and trashed=false",
  fields: 'files(id, name)',
}, (err, {data}) => {
    if (err) return console.log("The API returned an error: " + err);
    var files = data.files;
    if (files.length){
      console.log("Files:");
      files.map((file) => {
        console.log(`${file.name} (${file.id})`);
      });
    } else {
      console.log("No files found.");
    }
});*/

// WIP: Upload de arquivo fora de folder específico

/*var fileMetaData = {
  'name': 'text.txt'
};
var media = {
  mimeType: 'text/plain',
  body: fs.createReadStream('./text.txt')
};
drive.files.create({
  auth: jwToken,
  resource: fileMetaData,
  media: media,
  fields: 'id'
}, function (err, file){
  if (err){
    console.log(err);
  } else {
    console.log('File Id: ', file.id);
  }
});*/

//Connect ao banco
mongoose.connect(config.connectionString);

//Carrega os Models
const Map = require('./models/map');
// const User = require('./models/user');

//Carrega as Rotas
const indexRoute = require('./routes/index-routes');
const mapRoute = require('./routes/map-routes');
//const userRoute = require('./routes/user-routes');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended : false }));

// Habilita o CORS
/*app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, x-access-token');
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});*/

// Routing
app.use(express.static(path.join(__dirname, 'public')));
app.use('/maps', mapRoute);
app.use('/:mapName', (req, res) => res.sendFile(`${__dirname}/public/index.html`))
// app.use('/', indexRoute);
//app.use('/users', userRoute);

// Chatroom

var numUsers = 0;

// function onConnection(socket){
// 	//declaro todas as funções no nosso arquivo servidor. Emito o evento que eu quero junto com o seu parametro
//   socket.on('adding', (nodedata) => socket.broadcast.emit('adding', nodedata));
//   socket.on('removing', (nodeData) => socket.broadcast.emit('removing', nodeData));
//   socket.on('dragging', (data) => socket.broadcast.emit('dragging', data));
// }


// io.on('connection', onConnection);
var roomUsers = {};

io.on('connection', function (socket) {

  socket.on('storeName', function(data){
    if (roomUsers[data.mapTitle] === undefined){
      roomUsers[data.mapTitle] = 0;
    }
    socket['mapTitle'] = data.mapTitle;
    socket.join(data.mapTitle);
  })

  socket.on('autosave', function(data){
    console.log(data.title);
    maprep.update(data);    
  });

//  //declaro todas as funções no nosso arquivo servidor. Emito o evento que eu quero junto com o seu parametro
  socket.on('adding', (nodedata) => {
    socket.broadcast.to(socket.mapTitle).emit('adding', nodedata);
   });
  socket.on('addingCompundAlternative', (altdata) => socket.broadcast.to(socket.mapTitle).emit('addingCompundAlternative', altdata));
  socket.on('addingCharacterizer', (chardata) => socket.broadcast.to(socket.mapTitle).emit('addingCharacterizer', chardata));
  socket.on('removing', (nodeData) => socket.broadcast.to(socket.mapTitle).emit('removing', nodeData));
  socket.on('dragging', (data) => socket.broadcast.to(socket.mapTitle).emit('dragging', data));
  socket.on('renaming', (nodeDataRename) => socket.broadcast.to(socket.mapTitle).emit('renaming', nodeDataRename));
  socket.on('editingChar', (nodeCharEdit) => socket.broadcast.to(socket.mapTitle).emit('editingChar', nodeCharEdit));
  socket.on('updatingDescription', (nodeUpdateEdit) => socket.broadcast.to(socket.mapTitle).emit('updatingDescription', nodeUpdateEdit));





  socket.addedUser = false;

  // when the client emits 'new message', this listens and executes
  socket.on('new message', function (data) {
    // we tell the client to execute 'new message'
    socket.broadcast.to(socket.mapTitle).emit('new message', {
      username: socket.username,
      message: data
    });
  });

  // when the client emits 'add user', this listens and executes
  socket.on('add user', function (username) {
    if (socket.addedUser) return;

    // we store the username in the socket session for this client
    socket.username = username;
    ++roomUsers[socket.mapTitle];
    socket.addedUser = true;
    socket.emit('login', {
      numUsers: roomUsers[socket.mapTitle]
    });
    // echo globally (all clients) that a person has connected
    socket.broadcast.to(socket.mapTitle).emit('user joined', {
      username: socket.username,
      numUsers: roomUsers[socket.mapTitle]
    });
  });

  // when the client emits 'typing', we broadcast it to others
  socket.on('typing', function () {
    socket.broadcast.to(socket.mapTitle).emit('typing', {
      username: socket.username
    });
  });

  // when the client emits 'stop typing', we broadcast it to others
  socket.on('stop typing', function () {
    socket.broadcast.to(socket.mapTitle).emit('stop typing', {
      username: socket.username
    });
  });

  // when the user disconnects.. perform this
  socket.on('disconnect', function () {
    if (socket.addedUser) {
      --roomUsers[socket.mapTitle];
      if (roomUsers[socket.mapTitle] === 0){
        delete roomUsers[socket.mapTitle];
      }

      // echo globally that this client has left
      socket.broadcast.to(socket.mapTitle).emit('user left', {
        username: socket.username,
        numUsers: roomUsers[socket.mapTitle]
      });
    }
  });
});

http.listen(port, () => console.log('Listening on port ' + port));
